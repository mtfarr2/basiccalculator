package com.example.mark.basiccalculator;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.math.BigDecimal;
import java.math.RoundingMode;


/**
 * A simple {@link Fragment} subclass.
 */
public class InputFragment extends Fragment {

    MainActivity main;

    Button btnClear;
    Button btnDecimal;
    Button btnEquals;
    Button btnAdd;
    Button btnSubtract;
    Button btnMultiply;
    Button btnDivide;
    Button btnZero;
    Button btnOne;
    Button btnTwo;
    Button btnThree;
    Button btnFour;
    Button btnFive;
    Button btnSix;
    Button btnSeven;
    Button btnEight;
    Button btnNine;

    String firstNumberString = "";
    String secondNumberString = "";
    String currentNumberString = "";
    String operation = "";
    String answer;

    public InputFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_input, container, false);

        main = (MainActivity) getActivity();

        btnClear = (Button) view.findViewById(R.id.btnClear);
        btnDecimal = (Button) view.findViewById(R.id.btnDecimal);
        btnEquals = (Button) view.findViewById(R.id.btnEquals);
        btnAdd = (Button) view.findViewById(R.id.btnAdd);
        btnSubtract = (Button) view.findViewById(R.id.btnSubtract);
        btnMultiply = (Button) view.findViewById(R.id.btnMultiply);
        btnDivide = (Button) view.findViewById(R.id.btnDivide);
        btnZero = (Button) view.findViewById(R.id.btnZero);
        btnOne = (Button) view.findViewById(R.id.btnOne);
        btnTwo = (Button) view.findViewById(R.id.btnTwo);
        btnThree = (Button) view.findViewById(R.id.btnThree);
        btnFour = (Button) view.findViewById(R.id.btnFour);
        btnFive = (Button) view.findViewById(R.id.btnFive);
        btnSix = (Button) view.findViewById(R.id.btnSix);
        btnSeven = (Button) view.findViewById(R.id.btnSeven);
        btnEight = (Button) view.findViewById(R.id.btnEight);
        btnNine = (Button) view.findViewById(R.id.btnNine);

        btnClear.setOnClickListener(onClickListener);
        btnDecimal.setOnClickListener(onClickListener);
        btnEquals.setOnClickListener(onClickListener);
        btnAdd.setOnClickListener(onClickListener);
        btnSubtract.setOnClickListener(onClickListener);
        btnMultiply.setOnClickListener(onClickListener);
        btnDivide.setOnClickListener(onClickListener);
        btnZero.setOnClickListener(onClickListener);
        btnOne.setOnClickListener(onClickListener);
        btnTwo.setOnClickListener(onClickListener);
        btnThree.setOnClickListener(onClickListener);
        btnFour.setOnClickListener(onClickListener);
        btnFive.setOnClickListener(onClickListener);
        btnSix.setOnClickListener(onClickListener);
        btnSeven.setOnClickListener(onClickListener);
        btnEight.setOnClickListener(onClickListener);
        btnNine.setOnClickListener(onClickListener);


        return view;
    }

    private View.OnClickListener onClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v) {
            switch(v.getId())
            {
                case R.id.btnClear:
                    clearScreen();
                break;
                case R.id.btnDecimal:
                    if(currentNumberString.indexOf('.') == -1)
                    {
                        modifyCurrentNumberString(".");
                    }
                break;
                case R.id.btnEquals:
                    calculate();
                break;
                case R.id.btnAdd:
                    prepareCalculation("add");
                break;
                case R.id.btnSubtract:
                    prepareCalculation("subtract");
                break;
                case R.id.btnMultiply:
                    prepareCalculation("multiply");
                break;
                case R.id.btnDivide:
                    prepareCalculation("divide");
                break;
                case R.id.btnZero:
                    if(!currentNumberString.equals("0"))
                    {
                        modifyCurrentNumberString("0");
                    }
                break;
                case R.id.btnOne:
                    modifyCurrentNumberString("1");
                break;
                case R.id.btnTwo:
                    modifyCurrentNumberString("2");
                break;
                case R.id.btnThree:
                    modifyCurrentNumberString("3");
                break;
                case R.id.btnFour:
                    modifyCurrentNumberString("4");
                break;
                case R.id.btnFive:
                    modifyCurrentNumberString("5");
                break;
                case R.id.btnSix:
                    modifyCurrentNumberString("6");
                break;
                case R.id.btnSeven:
                    modifyCurrentNumberString("7");
                break;
                case R.id.btnEight:
                    modifyCurrentNumberString("8");
                break;
                case R.id.btnNine:
                    modifyCurrentNumberString("9");
                break;
            }
        }
    };

    private void modifyCurrentNumberString(String modification)
    {
        if (currentNumberString.length() < 9)
        {
            currentNumberString = currentNumberString + modification;
        }

        main.displayCurrentNumber(currentNumberString);
    }

    private void clearScreen()
    {
        currentNumberString = "";
        firstNumberString = "";
        secondNumberString ="";
        operation = "";

        main.displayCurrentNumber(currentNumberString);
    }

    private void prepareCalculation(String operation)
    {
        if(this.operation.equals(""))
        {
            this.operation = operation;
            firstNumberString = currentNumberString;
            currentNumberString = "";
        }
        else
        {
            calculate();
            this.operation = operation;
            firstNumberString = answer.toString();
            secondNumberString = "";
            currentNumberString = "";
        }
    }

    private void calculate()
    {
        if(!firstNumberString.equals("") && !currentNumberString.equals("") && !operation.equals(""))
        {
            secondNumberString = currentNumberString;
            currentNumberString = "";

            BigDecimal bigFirstNumber = new BigDecimal(firstNumberString);
            BigDecimal bigSecondNumber = new BigDecimal(secondNumberString);
            BigDecimal bigResult;

            if(operation.equals("add"))
            {
                bigResult = bigFirstNumber.add(bigSecondNumber);

                answer = bigResult.toPlainString();
            }
            else if (operation.equals("subtract"))
            {
                bigResult = bigFirstNumber.subtract(bigSecondNumber);
                answer = bigResult.toPlainString();
            }
            else if (operation.equals("multiply"))
            {
                bigResult = bigFirstNumber.multiply(bigSecondNumber);
                answer = bigResult.toPlainString();
            }
            else if (operation.equals("divide"))
            {
                bigResult = bigFirstNumber.divide(bigSecondNumber);
                answer = bigResult.toPlainString();
            }

            main.displayCurrentNumber(answer);

        }
    }
}
